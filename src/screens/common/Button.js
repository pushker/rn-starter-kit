import React from 'react';
import {
    Text,
    StyleSheet,
    TouchableOpacity,
} from 'react-native';

import { moderateScale } from '../../helpers/ResponsiveFonts';
import color from '../../constant/colors';

const Button = (props) => {
    return (
        <>
            <TouchableOpacity onPress={props.onPress} style={[styles.pv_button,{ borderColor: props.borderColor }, { borderWidth: props.borderWidth }, { marginTop: props.marginTop }, { marginRight: props.marginRight }, { backgroundColor: props.bgColor }, { width: props.width }, { height: props.height }, { flex: props.flex }, { borderRadius: moderateScale(props.borderRadius) }, { borderWidth: props.borderWidth }, { borderColor: props.borderColor, shadowColor: props.shadowColor ? props.shadowColor : "rgba(0, 0, 0, .4)" }]}>
                <Text style={[{ paddingTop: props.textPaddingTop }, {alignSelf: 'center', fontSize: moderateScale(props.fontSize), color: props.color ? props.color : color.White, fontFamily: props.fontFamily ? props.fontFamily : "HelveticaNeue",textTransform:'uppercase' }]}>{props.content}</Text>
            </TouchableOpacity>
        </>
    );
}
export default Button;
const styles = StyleSheet.create({
    pv_button: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        //Add some shadows
        shadowColor: 'rgba(0,0,0, .4)', // IOS
        shadowOffset: { height: 1, width: 1 }, // IOS
        shadowOpacity: 0.36, // IOS
        shadowRadius: moderateScale(2), //IOS
        elevation: 2, // Android
    }
});
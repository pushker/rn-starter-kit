import React, { Fragment } from "react";
import { SafeAreaView } from "react-native";
import colors from '../../constant/colors';

const SafeArea = props => (
    <Fragment>
        <SafeAreaView style={{ flex: 0, backgroundColor: colors.White }} />
        <SafeAreaView
            style={{
                flex: 1,
                backgroundColor: props.color
            }}
        >
            {props.children}
        </SafeAreaView>
    </Fragment>
);

export default SafeArea;
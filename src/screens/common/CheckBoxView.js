import React from 'react';
import { StyleSheet } from 'react-native';
import { moderateScale } from '../../helpers/ResponsiveFonts';
import { CheckBox } from 'react-native-elements';
import colors from '../../constant/colors';

export default function CheckBoxView(props) {
    return (
        <>
            <CheckBox style={styles.toolcheckbox}
                title={props.title}
                // checkedIcon={'checked'}
                // uncheckedIcon='circle-o'
                checkedColor="#000000"
                uncheckedColor="#EFF4F7"
                // checked={true}
                // textStyle={{ fontSize: moderateScale(21), color: 'white', opacity: getSingleValue(props.checked, props.title) ? moderateScale(0.4) : 1 }}
                size={20}
                containerStyle={{ width: '90%', paddingBottom: 5, paddingRight: 0, paddingLeft: 0, paddingTop: 5, margin: 0, backgroundColor: 'transparent', borderWidth: 0 }}
            />
        </>
    );
}
const styles = StyleSheet.create({
    toolcheckbox: {
        margin: 0,
        padding: 0,
        height: 10,
        padding: 0,
        margin: 0,
        marginLeft: -5,
        backgroundColor: '#EFF4F7'
    }
});
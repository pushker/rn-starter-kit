import { LOGIN } from './actionTypes';

export const login = (loginDetails) => {
    return {
        type: LOGIN,
        payload: loginDetails
    }
}
import React from "react";
import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

//screens of the application
import Home from '../screens/components/home';
import Login from '../screens/components/login';
const Stack = createStackNavigator();

export default function Navigations() {
    return (
        <NavigationContainer>
            <Stack.Navigator headerMode="none" initialRouteName={'Login'} screenOptions={{
                headerTransparent: true,
                gestureEnabled: false
            }}>
                <Stack.Screen
                    name="Login"
                    component={Login}
                />
                <Stack.Screen
                    name="Home"
                    component={Home}
                />
                
            </Stack.Navigator>
        </NavigationContainer>
    );
}
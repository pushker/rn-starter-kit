import React from 'react';
import { View, Text, StyleSheet, TextInput, KeyboardAvoidingView, Platform } from 'react-native';
import CheckBoxView from '../../common/CheckBoxView';
import colors from '../../../constant/colors';
import Button from '../../common/Button';
import { moderateScale } from '../../../helpers/ResponsiveFonts';

export default function Login() {
    return (
        <View style={styles.mainContainer}>
            <View style={styles.imageContainer}>
            </View>
            <KeyboardAvoidingView behavior="position" keyboardVerticalOffset={Platform.OS === "ios" ? moderateScale(-200) : moderateScale(-285)} style={styles.loginContainer}>
                <Text style={styles.titleStyle}>Phone Number</Text>
                <TextInput
                    autoCapitalize={"none"}
                    style={styles.lgn_txtInput}
                    placeholder={"PHONE NUMBER"}
                    secureTextEntry={true}
                    underlineColorAndroid={"rgba(0,0,0,0)"}
                    autoCorrect={false}
                    placeholderTextColor="#CECECE" />
                <Text style={styles.titleStyle}>Set Password</Text>
                <TextInput
                    autoCapitalize={"none"}
                    style={styles.lgn_txtInput}
                    placeholder={"SET PASSWORD"}
                    secureTextEntry={true}
                    underlineColorAndroid={"rgba(0,0,0,0)"}
                    autoCorrect={false}
                    placeholderTextColor="#CECECE" />
                <View style={styles.checkboxStyle}>
                    <CheckBoxView title={'I agree to the terms of services'} />
                </View>
                <Button bgColor="#F5637C" width={moderateScale(180)} fontSize={moderateScale(12)} height={moderateScale(42)} borderRadius={moderateScale(50)} color={colors.White} content="Sign up for free" />

            </KeyboardAvoidingView>
        </View>
    );
}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1, backgroundColor: colors.White,
    },
    loginContainer: {
        justifyContent: "space-between", alignSelf: "center", width: moderateScale(327), flex: 0.5,
        marginLeft: 'auto',
        marginRight: 'auto'
    },
    imageContainer: {
        flex: 0.2
    },
    lgn_txtInput: {
        alignSelf: "center",
        width: moderateScale(327),
        height: moderateScale(42),
        borderRadius: moderateScale(5),
        backgroundColor: '#EFF4F7',
        fontFamily: 'HelveticaNeue',
        color: 'black',
        padding: moderateScale(10),
        fontSize: moderateScale(17),
        borderColor: colors.Black,
        marginBottom: moderateScale(10)
    },
    titleStyle: {
        color: colors.Black,
        fontSize: 14,
        fontWeight: 'bold',
        textTransform: 'uppercase',
        marginBottom: 10
    }, checkboxStyle: {
        marginLeft: moderateScale(-5),
        marginBottom:moderateScale(10)
    }
})
import React from 'react';
import { View,Text,SafeAreaView } from  'react-native';
import { useNavigation } from '@react-navigation/native';

export default function Home(){
    const navigation = useNavigation();
    function loginNavigation() {
        navigation.navigate('Login');
    }
    return (
        <SafeAreaView style={{flex: 1}}>
            <View style={{ flex:1,justifyContent:'center', alignItems:'center'}}>
                <Text onPress={loginNavigation}>Home Page</Text>
            </View>
        </SafeAreaView>
    )
}
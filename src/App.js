import React from "react";
import { Provider } from "react-redux";
import { store, persistor } from "../store";
import { YellowBox } from "react-native";
import Nagivations from "./routes/Navigations"
import { PersistGate } from 'redux-persist/lib/integration/react';
import SafeArea from './screens/common/SafeArea';
YellowBox.ignoreWarnings(["Warning: ReactNative.createElement"]);
console.disableYellowBox = true;
console.reportErrorsAsExceptions = false;

export default function App() {
  return (
    <React.Fragment>
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <SafeArea>
            <Nagivations />
          </SafeArea>
        </PersistGate>
      </Provider>
    </React.Fragment>
  );
}
/*
color constants
 * */
"use strict";

let colors = {
    Primary: "#0C2394",
    backgroundColorLogin: "#101D94",
    lightGrey: "#EBEBEB",
    Primary2: "#ebebeb",
    Secondary: "rgb(252, 142, 49)",
    Yellow: "#F6CF65",
    DarkGray: "#393B3B",
    LightGray: "#B7B7B7",
    ChatTextColor: "##37474F",
    Transparent: "transparent",
    White: "#FFFFFF",
    Black: "#000000",
    placehoder: "#707070",
    listBorder: "#B4C9CC",
    green: "#8BCC82",
    red: "#e64c46",
    orange: "orange",
    transparent: "transparent",
    gray: "#A9AFAF",
    menuItemTxt: "#393B3B",
    fadeBorder: "#e0e0e0",
    ocen: "rgb(175, 207, 237)",
    lightYellow: "#fcfaae",
    DarkBlack: "#000000",
    ChatBlueColor: "#117DFF",
    ChatFooterColor: "#ECEFF0",
    BoxWrapperColor: "#F5F5F5",
    skyBlue: "#5b99bf",
    lightGreen: "#23D123",
    barBlue: "#4ba2c6",
    greenBar: "#1CA421",
    lineChart: "#ffc74a",
    yellow: "#ffc107",
    bgColor: "#007bff",
    bgColor2: "#28a745",
    mangenta: "#784d8e",
    grayTxt: "#898989",
    purpalColor: "#784D8E",
    tabBackgroundColor: "#f9f9f9",
    listBlue: "#2c89b9",
    lightBlack: '#383838'
};

module.exports = colors;